# command-center-bridge

A bridge solution for other programmable banking solutions that enables a single point of contact for approval determination and secure transaction forwarding to other APIs.

## TODO: next steps

*Roughly prioritized, all PRs welcome!*

* deploy with a custom domain
* swagger-like documentation
* add unit testing for handler and layer logic
* provide a web interface for authentication and jwt-protected functions
* OAUTH support + optional passwords
* getter and setter endpoints for
  * individual updates to forwards
* improve approval performance
  * target response time order of 1-1.5s on cold start so that warmup is unnecessary
* enable preauthorization fine-tuning eg. category / vendor
* failed transaction forwarding retries
  * for individual forwards
* implement weekly and custom limits
* transaction forward object mapping
  * provide mapping to forward definition eg `{ "completed": "finished", "merchant.category": "merchantCategory" }`
* [create metric dashboard programmatically](https://medium.com/poka-techblog/cloudwatch-dashboards-as-code-the-right-way-using-aws-cdk-1453309c5481)

## Overview

A bridge for "command centers" that

* performs card authorization logic
  * enables users to update transaction rules and limits
  * enables users to preauthorize individual transactions
* forwards data to private services
  * reduces origin overhead when multiple destinations are desirable

## Build and deploy

The Command Center Bridge is constructed in conjunction with [aws-cdk-js-dev-guide](https://github.com/therightstuff/aws-cdk-js-dev-guide). For documentation relating to the layout and usage of a CDK project - in particular regarding lambda layer setup - it's recommended to use [that project's README.md](https://github.com/therightstuff/aws-cdk-js-dev-guide) as a starting point.

### Quick start

* Create a programmatic user in IAM with admin permissions
* If you're using visual studio code (recommended), [configure aws toolkit](https://docs.aws.amazon.com/toolkit-for-vscode/latest/userguide/setup-toolkit.html)
* Set up credentials with the profile ID "default"
* Get your 12 digit account ID from My Account in the console

You'll need to update `stages.json`, `regions.json` and `credentials.json` in the `lib` folder with your desired stages and regions, your account ID and your credentials.

Once you're set up:

* `npm install -g cdk`: to make the cdk command available globally
* `npm install`: to install the project dependencies
* `npm run build`: to compile and lint your project and layer code
* `npm run synth`: to build and synthesize the cloudformation templates.

Once synthesized, you will be able to deploy with:

* `cdk deploy <stack_name>`: this will deploy your changes to the cloud, and will output your api root endpoint(s) to the console when complete.

To enable environment-agnostic deployments, run `cdk bootstrap` before `cdk deploy`, but note that configuring specific regions is probably the safer practice.

## Transaction authorization

* Update spend limits
  * Standard transaction limit
  * Daily
  * Weekly
  * Category
  * Vendor

* Preauthorize a transaction
  * Maximum amount
  * Time limit
  * Category
  * Vendor

* Notification on every transaction with remaining personal budgets

### Transaction forwarding

Each card has a `forwards` configuration, when a transaction is processed by the Command Center Bridge it will be forwarded to those services.

On transaction completion (indicated by `completed: true` in the transaction object):

* the closest matching pre-authorization will be removed
* expired preauthorizations will be swept
* relevant card limits will be updated

Forwarding definitions are encrypted with the API key which is never stored, if the API key is lost the forwards will need to be redefined.

## Endpoint overview

See the [sample Postman collection](./command-center-bridge.postman_collection.json) for usage (**yes, we definitely need swagger-like documentation**)

unprotected lambda functions:

* register with email / mobile
* verify email / number + otp
* initiate login with email / mobile
* complete login with email / mobile + otp
* retrieve budget state

jwt token protected:

* renew jwt token
* deregister email / mobile
* deactivate account
* create card
  * incl. account id, last four digits of card, forwarding definitions
  * regenerate api key per card
    * return key but store only hash with account id + last 4 digits
    * allow multiple keys for rotation with expiration
* set forwards (requires apiKey in queryString)
  * forwards is a document containing multiple named forwarding definitions.
    each definition incl. url, authentication method, credentials. posted
    data will be passed through as-is
* update forwards (requires apiKey in queryString)
  * a single named forwarding definition that will overwrite an existing definition
* retrieve transactions
  * filtered / paginated

api key protected:

(requests authenticated with card id and api key by checking against hash of card id and api key)

* request approval
* update limits (used/date for daily/weekly/monthly limits, balance for reserve limits)
* relay transaction
* (not implemented) update card limits and usage

triggered by other lambdas:

* user-defined transaction forwarding
