const aws = require('aws-sdk');
const cards = require('/opt/nodejs/ddb-layer/cards');
const sqs = new aws.SQS();
const utils = require('/opt/nodejs/utility-layer/utils');

const LIMIT_TYPES = require('/opt/nodejs/card-layer/limit-types');

const TRANSACTION_FORWARD_QUEUE_URL = process.env.TRANSACTION_FORWARD_QUEUE_URL;

function submitTransaction(transaction) {
    return new Promise((resolve, reject) => {
        const params = {
            QueueUrl: TRANSACTION_FORWARD_QUEUE_URL,
            // MessageBody must be a string
            MessageBody: JSON.stringify(transaction)
        };

        // push the message object to the queue
        sqs.sendMessage(params).promise()
        .then(() => {
            // return success
            resolve(true);
        })
        .catch(err => {
            // we write an error log
            console.error(err);
            // return failure
            resolve(false);
        })
    });
}

function authorizeTransaction({ card, payload }) {
    return new Promise((resolve, reject) => {

        let today = (new Date()).getDate();
        let thisMonth = (new Date()).getMonth();

        let cardLimits = card.cardLimits;

        // check pre-authorization for match
        // find the closest usable preauthorization
        let diff = NaN;
        let matchedIndex = -1;
        for (let i in cardLimits.preauthorized) {
            let limit = cardLimits.preauthorized[i];
            // ignore expired preauthorizations
            if (limit.expiration >= Math.floor(Date.now() / 1000)) {
                // TODO match on category/vendor if provided,
                //      if there's a match and the limit isn't
                //      high enough then reject
                let newDiff = limit.centsAmount - payload.centsAmount;
                if (newDiff >= 0) {
                    if (isNaN(diff) || newDiff < diff ) {
                        matchedIndex = i;
                        diff = newDiff;
                    }
                }
            }
        }
        if (!isNaN(diff)) {
            return resolve(true);
        }

        // check regular limits
        for (let i in cardLimits.regular) {
            let limit = cardLimits.regular[i];
            let limitUsage = cardLimits.usage[limit.type];
            switch (limit.type) {
                case LIMIT_TYPES.TRANSACTION:
                    if (limit.centsAmount < payload.centsAmount) {
                        return resolve(false);
                    }
                    break;
                case LIMIT_TYPES.DAILY:
                    // if it's the same day, subtract the used amount
                    if (limitUsage.date == today) {
                        limit.centsAmount -= limitUsage.used;
                    }
                    if (limit.centsAmount < payload.centsAmount) {
                        return resolve(false);
                    }
                    break;
                case LIMIT_TYPES.WEEKLY:
                    // TODO implement weekly limit
                    break;
                case LIMIT_TYPES.MONTHLY:
                    // if it's the same day, subtract the used amount
                    if (limitUsage.month == thisMonth) {
                        limit.centsAmount -= limitUsage.used;
                    }
                    if (limit.centsAmount < payload.centsAmount) {
                        return resolve(false);
                    }
                    break;
            }
        }

        // no limits hit, approve
        return resolve(true);
    });
}

exports.approve = async (event) => {
    // warmup calls do not have event.pathParameters defined
    if (!event.pathParameters) {
        console.log('warmup call detected, ending');
        return;
    }

    return new Promise((resolve, reject) => {
        let cardId = event.pathParameters.cardId;
        let apiKey = event.headers.apiKey;
        if (!(cardId && apiKey)) {
            return resolve(
                utils.formatErrorResponse(400, new Error("invalid query"))
            );
        }
        cards.authenticate({ cardId, apiKey })
        .then(card => {
            let payload = null;
            try {
                payload = utils.processEventBody({
                    body: event.body,
                    required: ['centsAmount']
                });
            } catch (err) {
                return resolve(utils.formatErrorResponse(400, err));
            }

            let authorizeTransactionPromise = authorizeTransaction({ card, payload });

            // submit approval request for forwarding
            // NOTE: we won't update any of the card's limits until
            //       we receive the completed transaction event
            let submitTransactionPromise = submitTransaction({
                "cardId": card.cardId,
                "apiKey": apiKey,
                "body": payload
            });

            Promise.all([authorizeTransactionPromise, submitTransactionPromise])
            .then(result => {
                if (result.indexOf(false) >= 0) {
                    return resolve(utils.createResponse({
                        "statusCode": 200,
                        "body": {
                            "success": false
                        }
                    }));
                }
                return resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true
                    }
                }));
            });
        })
        .catch((err) => {
            console.log(err);
            resolve(utils.formatErrorResponse(401, new Error("Authentication failed")));
        });
    });
};