
const cards = require('/opt/nodejs/ddb-layer/cards');
const jwt = require('/opt/nodejs/utility-layer/jwt');
const utils = require('/opt/nodejs/utility-layer/utils');

const LIMIT_TYPES = require('/opt/nodejs/card-layer/limit-types');

exports.addCard = async (event) => {
    return new Promise((resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(({ userId }) => {
            let payload = null;
            try {
                payload = utils.processEventBody({
                    body: event.body,
                    required: ['cardName', 'cardNumber']
                });
            } catch (err) {
                return resolve(utils.formatErrorResponse(400, err));
            }
            cards.createEntry({
                userId,
                cardName: payload.cardName,
                cardNumber: payload.cardNumber
            })
            .then(({ cardId, apiKey }) => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true,
                        "cardId": cardId,
                        "apiKey": apiKey
                    }
                }));
            })
            .catch(err => {
                console.error(err);
                return resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Failed to add the card.`
                ));
            });
        })
        .catch(authErr => {
            resolve(utils.formatErrorResponse(
                authErr.statusCode,
                null,
                authErr.message
            ));
        });
    });
};

exports.listCards = async (event) => {
    return new Promise((resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(({ userId, device}) => {
            cards.list({
                userId
            })
            .then((cards) => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true,
                        "cards": cards
                    }
                }));
            })
            .catch(err => {
                console.error(err);
                return resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Unable to list cards.`
                ));
            });
        })
        .catch(authErr => {
            resolve(utils.formatErrorResponse(
                authErr.statusCode,
                null,
                authErr.message
            ));
        });
    });
};

exports.generateApiKey = async (event) => {
    return new Promise((resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(() => {
            let cardId = event.pathParameters.cardId;
            let apiKey = event.queryStringParameters.apiKey;
            let clearForwards = event.queryStringParameters.clearForwards;
            if (!(cardId && (apiKey || clearForwards))) {
                return resolve(utils.formatErrorResponse(
                    400,
                    null,
                    "invalid query, cardId and either apiKey / clearForwards required"
                ));
            }
            console.log(`attempting to regenerate api key for ${cardId}`);
            cards.generateApiKey({ cardId, apiKey, clearForwards })
            .then((apiKey) => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true,
                        "apiKey": apiKey
                    }
                }));
            })
            .catch(err => {
                console.error(err);
                return resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `An error occurred updating the api key.`
                ));
            });
        })
        .catch(authErr => {
            resolve(utils.formatErrorResponse(
                authErr.statusCode,
                null,
                authErr.message
            ));
        });
    });
};

exports.updateUsage = async (event) => {
    return new Promise((resolve, reject) => {
        let cardId = event.pathParameters.cardId;

        let apiKey = event.headers.apiKey;
        if (!(cardId && apiKey)) {
            return resolve(utils.formatErrorResponse(400, new Error("invalid query")));
        }
        cards.authenticate({ cardId, apiKey })
        .then(card => {
            let payload = null;
            try {
                payload = utils.processEventBody({
                    body: event.body,
                    required: ['centsAmount']
                });
            } catch (err) {
                return resolve(utils.formatErrorResponse(400, err));
            }

            card.cardLimits.usage = cards.updateUsage({
                usage: card.cardLimits.usage,
                centsAmount: payload.centsAmount,
                // if reset flag is set, override (set usage to provided value)
                override: payload.reset
            });

            cards.updateEntry({ cardId: card.cardId, cardLimits: card.cardLimits })
            .then(() => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true
                    }
                }));
            })
            .catch((err) => {
                console.error(err);
                resolve(utils.formatErrorResponse(500, err));
            });
        })
        .catch((err) => {
            console.log(err);
            resolve(utils.formatErrorResponse(
                401,
                null,
                "Authentication failed"
            ));
        });
    });
};

exports.getUsage = async (event) => {
    return new Promise((resolve, reject) => {
        let cardId = event.pathParameters.cardId;

        let apiKey = event.headers.apiKey;
        if (!(cardId && apiKey)) {
            return resolve(utils.formatErrorResponse(400, new Error("invalid query")));
        }
        cards.authenticate({ cardId, apiKey })
        .then(card => {
            let today = (new Date()).getDate();
            let thisMonth = (new Date()).getMonth();

            let usage = {};
            if (card.cardLimits.usage[LIMIT_TYPES.DAILY].date == today) {
                usage[LIMIT_TYPES.DAILY] = card.cardLimits.usage[LIMIT_TYPES.DAILY].used;
            } else {
                usage[LIMIT_TYPES.DAILY] = 0;
            }
            if (card.cardLimits.usage[LIMIT_TYPES.MONTHLY].month == thisMonth) {
                usage[LIMIT_TYPES.MONTHLY] = card.cardLimits.usage[LIMIT_TYPES.MONTHLY].used;
            } else {
                usage[LIMIT_TYPES.MONTHLY] = 0;
            }

            resolve(utils.createResponse({
                "statusCode": 200,
                "body": {
                    "success": true,
                    "usage": usage
                }
            }));
        })
        .catch((err) => {
            console.log(err);
            resolve(utils.formatErrorResponse(
                401,
                null,
                "Authentication failed"
            ));
        });
    });
};

exports.setCardLimits = async (event) => {
    return new Promise((resolve, reject) => {
        let cardId = event.pathParameters.cardId;

        let apiKey = event.headers.apiKey;
        if (!(cardId && apiKey)) {
            return resolve(utils.formatErrorResponse(400, new Error("invalid query")));
        }
        cards.authenticate({ cardId, apiKey })
        .then(card => {
            let payload = null;
            try {
                payload = utils.processEventBody({
                    body: event.body
                });
            } catch (err) {
                return resolve(utils.formatErrorResponse(400, err));
            }

            let cardLimits = card.cardLimits;

            // validate limits
            for (let i in payload) {
                let limit = payload[i];
                let err = null;
                switch (limit.type) {
                    case LIMIT_TYPES.TRANSACTION:
                    case LIMIT_TYPES.DAILY:
                    case LIMIT_TYPES.MONTHLY:
                        if (!limit.centsAmount) {
                            err = `${limit.type} limit must include centsAmount`;
                        }
                        break;
                    case LIMIT_TYPES.WEEKLY:
                        // TODO configure weekly with startOfWeek || 0 (sunday)
                        err = `weekly limits not implemented`;
                        break;
                    default:
                        err = `limit '${limit.type}' not implemented`;
                }

                if (err) {
                    return resolve(utils.formatErrorResponse(
                        400,
                        null,
                        err
                    ));
                }
            };

            cardLimits.regular = payload;

            cards.updateEntry({ cardId, cardLimits })
            .then(() => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true
                    }
                }));
            })
            .catch(err => {
                console.error(err);
                return resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Failed to update card limits.`
                ));
            });
        })
        .catch((err) => {
            console.log(err);
            resolve(utils.formatErrorResponse(
                401,
                null,
                "Authentication failed"
            ));
        });
    });
};

exports.getCardLimits = async (event) => {
    return new Promise((resolve, reject) => {
        let cardId = event.pathParameters.cardId;

        let apiKey = event.headers.apiKey;
        if (!(cardId && apiKey)) {
            return resolve(utils.formatErrorResponse(400, new Error("invalid query")));
        }
        cards.authenticate({ cardId, apiKey })
        .then(card => {
            let cardLimits = {};
            cardLimits.preauthorized = card.cardLimits.preauthorized;
            cardLimits.regular = card.cardLimits.regular;

            resolve(utils.createResponse({
                "statusCode": 200,
                "body": {
                    "success": true,
                    "limits": cardLimits
                }
            }));
        })
        .catch((err) => {
            console.log(err);
            resolve(utils.formatErrorResponse(
                401,
                null,
                "Authentication failed"
            ));
        });
    });
};

exports.addPreauthorization = async (event) => {
    return new Promise((resolve, reject) => {
        let cardId = event.pathParameters.cardId;

        let apiKey = event.headers.apiKey;
        if (!(cardId && apiKey)) {
            return resolve(utils.formatErrorResponse(400, new Error("invalid query")));
        }
        cards.authenticate({ cardId, apiKey })
        .then(card => {
            let payload = null;
            try {
                payload = utils.processEventBody({
                    body: event.body
                });
            } catch (err) {
                return resolve(utils.formatErrorResponse(400, err));
            }

            let cardLimits = card.cardLimits;

            let preauthorization = payload;
            if (!(preauthorization.centsAmount && preauthorization.expiration)) {
                return resolve(utils.formatErrorResponse(
                    400,
                    null,
                    "preauthorizations must include centsAmount and expiration"
                ));
            }
            cardLimits.preauthorized.push(preauthorization);

            cards.updateEntry({ cardId, cardLimits })
            .then(() => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true
                    }
                }));
            })
            .catch(err => {
                console.error(err);
                return resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Failed to update card limits.`
                ));
            });
        })
        .catch((err) => {
            console.log(err);
            resolve(utils.formatErrorResponse(
                401,
                null,
                "Authentication failed"
            ));
        });
    });
};

exports.deletePreauthorization = async (event) => {
    return new Promise((resolve, reject) => {
        let cardId = event.pathParameters.cardId;

        let apiKey = event.headers.apiKey;
        if (!(cardId && apiKey)) {
            return resolve(utils.formatErrorResponse(400, new Error("invalid query")));
        }
        cards.authenticate({ cardId, apiKey })
        .then(card => {
            let payload = null;
            try {
                payload = utils.processEventBody({
                    body: event.body
                });
            } catch (err) {
                return resolve(utils.formatErrorResponse(400, err));
            }

            let cardLimits = card.cardLimits;

            // TODO for each limit, remove matching limit in card.cardLimits
            return resolve(utils.formatErrorResponse(
                500,
                null,
                "not implemented"
            ));
        })
        .catch((err) => {
            console.log(err);
            resolve(utils.formatErrorResponse(
                401,
                null,
                "Authentication failed"
            ));
        });
    });
};