const aws = require('aws-sdk');
const cards = require('/opt/nodejs/ddb-layer/cards');
const sqs = new aws.SQS();
const utils = require('/opt/nodejs/utility-layer/utils');

const LIMIT_TYPES = require('/opt/nodejs/card-layer/limit-types');

const TRANSACTION_FORWARD_QUEUE_URL = process.env.TRANSACTION_FORWARD_QUEUE_URL;

function submitTransaction(transaction) {
    return new Promise((resolve, reject) => {
        const params = {
            QueueUrl: TRANSACTION_FORWARD_QUEUE_URL,
            // MessageBody must be a string
            MessageBody: JSON.stringify(transaction)
        };

        // push the message object to the queue
        sqs.sendMessage(params).promise()
        .then(() => {
            // return success
            resolve(true);
        })
        .catch(err => {
            // we write an error log
            console.error(err);
            // return failure
            resolve(false);
        })
    });
}

function completeTransaction({ card, payload }) {
    return new Promise((resolve, reject) => {
        let cardLimits = card.cardLimits;

        // check pre-authorization for match
        // find the closest usable preauthorization
        let diff = NaN;
        let matchedIndex = -1;
        for (let i in cardLimits.preauthorized) {
            let limit = cardLimits.preauthorized[i];
            // ignore expired preauthorizations
            // allow an additional 30s in case the preauthorization expired the second it was approved
            if (limit.expiration >= (Math.floor(Date.now() / 1000) + 30)) {
                // TODO match on category/vendor if provided,
                //      if there's a match and the limit isn't
                //      high enough then reject
                let newDiff = limit.centsAmount - payload.centsAmount;
                if (newDiff >= 0) {
                    if (isNaN(diff) || newDiff < diff ) {
                        matchedIndex = i;
                        diff = newDiff;
                    }
                }
            }
        }
        if (!isNaN(diff)) {
            // remove the matching preauthorization from the card limit
            let removed = cardLimits.preauthorized.splice(matchedIndex, 1);
            console.log(`removed preauthorization:`, removed);
        }
        // sweep expired preauthorizations
        let validPreauthorizations = [];
        for (let i in cardLimits.preauthorized) {
            let limit = cardLimits.preauthorized[i];
            if (limit.expiration >= Math.floor(Date.now() / 1000)) {
                validPreauthorizations.push(limit);
            } else {
                console.log(`cleared expired preauthorization:`, limit);
            }
        }

        // update usage (preauthorized transactions count towards regular limits)
        cardLimits.usage = cards.updateUsage({
            usage: cardLimits.usage, centsAmount: payload.centsAmount
        });

        cards.updateEntry({ cardId: card.cardId, cardLimits })
        .then(() => { resolve(true); })
        .catch((err) => {
            console.error(err);
            resolve(false);
        });
    });
}

exports.relay = async (event) => {
    return new Promise((resolve, reject) => {
        let cardId = event.pathParameters.cardId;

        let apiKey = event.headers.apiKey;
        if (!(cardId && apiKey)) {
            return resolve(utils.formatErrorResponse(400, new Error("invalid query")));
        }
        cards.authenticate({ cardId, apiKey })
        .then(card => {
            let payload = null;
            try {
                payload = utils.processEventBody({
                    body: event.body,
                    required: ['centsAmount']
                });
            } catch (err) {
                return resolve(utils.formatErrorResponse(400, err));
            }

            let promises = [];

            // if transaction completed successfully, match and update pre-auth/update card limits
            if (payload.completed) {
                promises.push(completeTransaction({ card, payload }));
            }

            // submit transaction for forwarding
            promises.push(
                submitTransaction({
                    "cardId": card.cardId,
                    "apiKey": apiKey,
                    "body": payload
                })
            );

            Promise.all(promises)
            .then(result => {
                if (result.indexOf(false) >= 0) {
                    return resolve(utils.createResponse({
                        "statusCode": 200,
                        "body": {
                            "success": false
                        }
                    }));
                }
                return resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true
                    }
                }));
            });
        })
        .catch((err) => {
            console.log(err);
            resolve(utils.formatErrorResponse(
                401,
                null,
                "Authentication failed"
            ));
        });
    });
};
