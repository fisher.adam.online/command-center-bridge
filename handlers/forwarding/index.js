const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const fetch = require('node-fetch');
const sfet = require('simple-free-encryption-tool');

const forwards = require('/opt/nodejs/ddb-layer/forwards');
const jwt = require('/opt/nodejs/utility-layer/jwt');
const utils = require('/opt/nodejs/utility-layer/utils');

const FORWARDS_TABLE = process.env.FORWARDS_TABLE_NAME;

let processForwardingEvent = async (record) => {
    const transaction = JSON.parse(record.body);
    // WARNING: do not log transaction as it contains the apiKey

    try {
        console.log(`looking up forwards for card ${transaction.cardId}`);

        // get must be called synchronously or it
        // will be killed as the function ends
        let result = await dynamodb.get({
            TableName: FORWARDS_TABLE,
            Key: {
                "cardId": transaction.cardId
            }
        }).promise();

        if (!result.Item) {
            console.log(`no forwards found for card ${transaction.cardId}`);
            return;
        };

        let forwards = result.Item.forwards;
        try {
            forwards = JSON.parse(sfet.aes.decrypt(transaction.apiKey, forwards));
        } catch (err) {
            console.error(`unable to decrypt forwards`, err);
            return;
        }

        let promises = [];
        for (let i in forwards) {
            let forward = forwards[i];
            // set up request promises
            promises.push(
                new Promise((resolve, reject) => {
                    fetch(forward.url, {
                        method: 'post',
                        body: JSON.stringify(transaction.body), //forward.map(transaction.body),
                        headers: forward.headers
                    })
                    .then(res => {
                        let urlHost = (new URL(forward.url)).host;
                        if (res.ok) { // res.status >= 200 && res.status < 300
                            console.log(`successfully forwarded to ${urlHost} with response ${res.status}`);
                        } else {
                            console.error(`error forwarding to ${urlHost} with response ${res.status}: ${res.statusText}`);
                        }
                        resolve(true);
                    })
                    .catch(err => {
                        console.error(`error forwarding to ${forward.url}:`, err)
                        resolve(false);
                    });
                })
            );
        }
        await Promise.all(promises);
    } catch (err) {
        console.error(err);
    }
}

exports.forwarder = async (event) => {
    for (let record of event.Records) {
        await processForwardingEvent(record);
    }
}

exports.storeForwards = async (event) => {
    return new Promise((resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(({ userId, device}) => {
            let payload = null;
            try {
                payload = utils.processEventBody({
                    body: event.body
                });
            } catch (err) {
                return resolve(utils.formatErrorResponse(400, err));
            }
            let cardId = event.pathParameters.cardId;
            let apiKey = event.queryStringParameters.apiKey;
            forwards.createOrUpdateEntry({ cardId, apiKey, forwards: payload })
            .then(() => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true
                    }
                }));
            })
            .catch(err => {
                console.error(err);
                return resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Failed to update card forwarding configuration.`
                ));
            });
        })
        .catch(authErr => {
            resolve(utils.formatErrorResponse(
                authErr.statusCode,
                null,
                authErr.message
            ));
        });
    });
};

exports.listForwards = async (event) => {
    return new Promise((resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(({ userId, device }) => {
            let cardId = event.pathParameters.cardId;
            let apiKey = event.queryStringParameters.apiKey;

            forwards.getEntry({ cardId, apiKey })
            .then((forwards) => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true,
                        "forwards": forwards
                    }
                }));
            })
            .catch(err => {
                console.error(err);
                return resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Unable to list forwards.`
                ));
            });
        })
        .catch(authErr => {
            resolve(utils.formatErrorResponse(
                authErr.statusCode,
                null,
                authErr.message
            ));
        });
    });
};
