
const jwt = require('/opt/nodejs/utility-layer/jwt');
const utils = require('/opt/nodejs/utility-layer/utils');

exports.refreshToken = async (event) => {
    return new Promise((resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then(({ userId }) => {
            jwt.generateToken({ userId })
            .then((token) => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true,
                        "message": `Token renewal succeeded.`,
                        "token": token
                    }
                }));
            })
            .catch((err) => {
                console.error(err);
                resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Token renewal failed.`
                ));
            });
        })
        .catch(authErr => {
            resolve(utils.formatErrorResponse(
                authErr.statusCode,
                null,
                authErr.message
            ));
        });
    });
};