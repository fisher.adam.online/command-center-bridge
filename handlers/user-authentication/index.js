
const jwt = require('/opt/nodejs/utility-layer/jwt');
const login = require('/opt/nodejs/ddb-layer/login');
const otp = require('/opt/nodejs/otp-layer/otp');
const users = require('/opt/nodejs/ddb-layer/users');
const utils = require('/opt/nodejs/utility-layer/utils');

const DEVICE_TYPES = require('/opt/nodejs/otp-layer/device-types');

// deliberately ambiguous message to prevent registered user discovery
const AMBIGUOUS_DELIVERY_MESSAGE = "You should receive an OTP shortly.";

const PURPOSE_AUTHENTICATION = "user authentication"

function processEventBody({ body, required }) {
    payload = JSON.parse(body);
    if (!payload) throw new Error("No request body");

    // check that email or mobile (not both) in body
    if (!utils.xor([payload.email, payload.mobile])) {
        throw new Error("Request must include only an 'email' or a 'mobile' field, not both.");
    }
    for (let i in required) {
        let key = required[i];
        if (!payload[key]) {
            throw new Error(`Request must include '${key}'`);
        }
    }
    if (payload.email) {
        payload.device = payload.email;
        payload.deviceType = DEVICE_TYPES.EMAIL;
    } else {
        payload.device = payload.mobile;
        payload.deviceType = DEVICE_TYPES.MOBILE;
    }
    return payload;
}

function sendOTP({deviceType, device}) {
    return new Promise((resolve, reject) => {
        otp.createEntry({ device, purpose: PURPOSE_AUTHENTICATION })
        .then((value) => {
            otp.send({
                deviceType,
                device,
                subject: "Command Center Bridge: Your One-Time Password",
                body: `Your OTP is ${value}`
            })
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
}

exports.login = async (event) => {
    return new Promise((resolve, reject) => {
        let payload = null;
        try {
            payload = processEventBody({ body: event.body });
        } catch (err) {
            return resolve(utils.formatErrorResponse(400, err));
    }
        let loginFailureErrorHandler = (err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                500,
                err,
                `Login with ${payload.device} failed.`
            ));
        };
        let ambiguousResponse = utils.createResponse({
            "statusCode": 200,
            "body": {
                "success": true,
                "message": AMBIGUOUS_DELIVERY_MESSAGE
            }
        });
        // lookup device
        login.getEntry({ device: payload.device })
        .then(item => {
            if (!item) {
                // if device is not registered, simply return an ambiguous response
                console.error(`user attempted to login with unregistered device ${payload.device}`);
                resolve(ambiguousResponse);
            } else {
                // check if user entry exists, if not attempt to create it
                users.getEntry({ userId: item.userId })
                .then(user => {
                    if (!user) {
                        users.createEntry({
                            userId: item.userId,
                            device: payload.device,
                            deviceType: payload.deviceType
                        })
                        .then(() => {
                            console.log(`user ${item.userId} entry created successfully`);
                            continueLogin();
                        })
                        .catch(err => {
                            console.error(err);
                            loginFailureErrorHandler();
                        })
                    } else {
                        continueLogin();
                    }
                })
                .catch(err => {
                    console.error(`encountered error with user ${item.userId}`, err);
                    loginFailureErrorHandler();
                })

                let continueLogin = () => {
                    if (item.verified) {
                        // send otp email/sms
                        sendOTP({
                            deviceType: payload.deviceType,
                            device: payload.device
                        })
                        .then(() => {
                            resolve(utils.createResponse({
                                "statusCode": 200,
                                "body": {
                                    "success": true,
                                    "message": AMBIGUOUS_DELIVERY_MESSAGE
                                }
                            }));
                        })
                        .catch(loginFailureErrorHandler);
                    } else {
                        // if device is not verified, send a reminder to complete
                        // registration and return an ambiguous response
                        otp.send({
                            deviceType: payload.deviceType,
                            device: payload.device,
                            subject: "Command Center Bridge: Complete your registration",
                            body: `Someone has just attempted to login using this email
                                address, but the address has not yet been verified.`
                        })
                        .then(() => {
                            resolve(ambiguousResponse);
                        })
                        .catch((err) => {
                            console.error('error sending registration reminder:')
                            console.error(err);
                            resolve(ambiguousResponse);
                        });
                    }
                };
            }
        })
        .catch(err => {
            console.error(err);
            return resolve(utils.formatErrorResponse(
                500,
                err,
                `an error occurred while querying device ${payload.device}`
            ));
        })
    });
}

exports.authenticate = async (event) => {
    return new Promise((resolve, reject) => {
        // check that either email or mobile in body
        try {
            payload = processEventBody({ body: event.body, required: ['otp'] });
        } catch (err) {
            return resolve(utils.formatErrorResponse(400, err));
        }
        let authenticationFailureErrorHandler = (err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                null,
                `Login with ${payload.device} failed, OTP invalid / expired.`
            ));
        };
        // if otp not found or OTP expiration in the past then return "otp has expired"
        otp.getEntry({ device: payload.device, otp: payload.otp })
        .then(item => {
            // check otp has the correct purpose
            if (item.purpose != PURPOSE_AUTHENTICATION) {
                authenticationFailureErrorHandler(`otp purpose mismatch: ${item.purpose}`);
            } else {
                console.log(`successfully authenticated device ${payload.device}`);
                // generate JWT token, delete otp
                jwt.generateToken({ device: payload.device })
                .then((token) => {
                    otp.deleteEntry({
                        device: payload.device,
                        otp: payload.otp
                    })
                    .then(() => {
                        console.log('successfully deleted otp');
                        authenticationSuccessHandler();
                    })
                    .catch(err => {
                        console.error(err);
                        authenticationSuccessHandler();
                    })

                    let authenticationSuccessHandler = () => {
                        resolve(utils.createResponse({
                            "statusCode": 200,
                            "body": {
                                "success": true,
                                "message": `Authentication of ${payload.device} succeeded.`,
                                "token": token
                            }
                        }));
                    };
                })
                .catch(authenticationFailureErrorHandler);
            }
        })
        .catch(authenticationFailureErrorHandler);
    });
}
