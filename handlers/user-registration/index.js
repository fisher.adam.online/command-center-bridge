const jwt = require('/opt/nodejs/utility-layer/jwt');
const login = require('/opt/nodejs/ddb-layer/login');
const otp = require('/opt/nodejs/otp-layer/otp');
const users = require('/opt/nodejs/ddb-layer/users');
const utils = require('/opt/nodejs/utility-layer/utils');

const DEVICE_TYPES = require('/opt/nodejs/otp-layer/device-types');

// deliberately ambiguous message to prevent registered user discovery
const AMBIGUOUS_DELIVERY_MESSAGE = "You should receive an OTP shortly if this device is not already registered.";

const PURPOSE_REGISTRATION = "user registration"

function processEventBody({ body, required }) {
    payload = JSON.parse(body);
    if (!payload) throw new Error("No request body");

    // check that email or mobile (not both) in body
    if (!utils.xor([payload.email, payload.mobile])) {
        throw new Error("Request must include only an 'email' or a 'mobile' field, not both.");
    }

    // check for other required fields
    let vowels = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'];
    for (let i in required) {
        let key = required[i];
        if (!payload[key]) {
            let an = vowels.indexOf(key[i].charAt(0)) > -1;
            throw new Error(`Request must include a${an ? 'n' : ''} '${key}' field.`);
        }
    }

    if (payload.email) {
        payload.device = payload.email;
        payload.deviceType = DEVICE_TYPES.EMAIL;
    } else {
        payload.device = payload.mobile;
        payload.deviceType = DEVICE_TYPES.MOBILE;
    }
    return payload;
}

function sendOTP({deviceType, device}) {
    return new Promise((resolve, reject) => {
        otp.createEntry({ device, purpose: PURPOSE_REGISTRATION })
        .then((value) => {
            otp.send({
                deviceType,
                device,
                subject: "Command Center Bridge: Your One-Time Password",
                body: `Your OTP is ${value}`
            })
            .then(resolve)
            .catch(reject);
        })
        .catch(reject);
    });
}

exports.register = async (event) => {
    return new Promise((resolve, reject) => {
        let payload = null;
        try {
            payload = processEventBody({ body: event.body });
        } catch (err) {
            return resolve(utils.formatErrorResponse(400, err));
        }
        let registrationFailureErrorHandler = (err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                500,
                err,
                `Registration of ${payload.device} failed.`
            ));
        };
        // we use ambiguous responses to protect users from being identified by third-party registration attempts
        let ambiguousResponse = utils.createResponse({
            "statusCode": 200,
            "body": {
                "success": true,
                "message": AMBIGUOUS_DELIVERY_MESSAGE
            }
        });
        // lookup device
        login.getEntry({ device: payload.device })
        .then(item => {
            if (!item) {
                // first attempt at registration
                login.createEntry({ device: payload.device })
                .then(() => {
                    sendOTP({
                        deviceType: payload.deviceType,
                        device: payload.device
                    })
                    .then(() => {
                        resolve(ambiguousResponse);
                    })
                    .catch(registrationFailureErrorHandler);
                })
                .catch(registrationFailureErrorHandler);
            } else {
                if (item.verified) {
                    // send repeat registration warning email/sms
                    otp.send({
                        deviceType: payload.deviceType,
                        device: payload.device,
                        subject: "Command Center Bridge: Repeat registration warning",
                        body: `Someone has just attempted to register using this email address.`
                    })
                    .then(() => {
                        resolve(ambiguousResponse);
                    })
                    .catch((err) => {
                        console.error('error sending repeat registration warning:')
                        console.error(err);
                        resolve(ambiguousResponse);
                    });
                } else {
                    // send otp email/sms
                    sendOTP({
                        deviceType: payload.deviceType,
                        device: payload.device
                    })
                    .then(() => {
                        resolve(utils.createResponse({
                            "statusCode": 200,
                            "body": {
                                "success": true,
                                "message": AMBIGUOUS_DELIVERY_MESSAGE
                            }
                        }));
                    })
                    .catch(registrationFailureErrorHandler);
                }
            }
        })
        .catch(err => {
            console.error(err);
            return resolve(utils.formatErrorResponse(
                500,
                err,
                `an error occurred while querying device ${payload.device}`
            ));
        })
    });
}

exports.verify = async (event) => {
    return new Promise((resolve, reject) => {
        let payload = null;
        try {
            payload = processEventBody({ body: event.body, required: ['otp'] });
        } catch (err) {
            return resolve(utils.formatErrorResponse(400, err));
        }
        let verificationFailureErrorHandler = (err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err,
                `Verification of ${payload.device} failed, OTP invalid / expired.`
            ));
        };
        // if otp not found or OTP expiration in the past then return "otp has expired"
        otp.getEntry({ device: payload.device, otp: payload.otp })
        .then(item => {
            // check otp has the correct purpose
            if (item.purpose != PURPOSE_REGISTRATION) {
                verificationFailureErrorHandler(`otp purpose mismatch: ${item.purpose}`);
            } else {
                // update verified state, create user entry, delete otp
                // TODO TBD return jwt token?
                login.verifyEntry({ device: payload.device })
                .then((verifiedItem) => {

                    users.createEntry({
                        userId: verifiedItem.userId,
                        device: payload.device,
                        deviceType: payload.deviceType
                    })
                    .then(() => {
                        console.log(`user ${verifiedItem.userId} entry created successfully`);
                        deleteOtp();
                    })
                    .catch(err => {
                        console.error(`encountered error creating user ${verifiedItem.userId}`, err);
                        deleteOtp();
                    });

                    let deleteOtp = () => {
                        otp.deleteEntry({
                            device: payload.device,
                            otp: payload.otp
                        })
                        .then(() => {
                            console.log('otp deleted successfully');
                            verificationSuccessHandler();
                        })
                        .catch(err => {
                            console.error(err);
                            verificationSuccessHandler();
                        })
                    };

                    let verificationSuccessHandler = () => {
                        resolve(utils.createResponse({
                            "statusCode": 200,
                            "body": {
                                "success": true,
                                "message": `Verification of ${payload.device} succeeded.`
                            }
                        }));
                    };
                })
                .catch(verificationFailureErrorHandler);
            }
        })
        .catch(verificationFailureErrorHandler);
    });
}

exports.deregister = async (event) => {
    return new Promise((resolve, reject) => {
        jwt.authenticateToken({ requestHeaders: event.headers })
        .then((userId) => {
            // TODO check that device belongs to userId
            console.log(`authenticated call to deregister with user id ${userId}`)
            resolve(utils.formatErrorResponse(
                500,
                null,
                "not implemented"
            ));
        })
        .catch(authErr => {
            resolve(utils.formatErrorResponse(
                authErr.statusCode,
                null,
                authErr.message
            ));
        });
    });
};