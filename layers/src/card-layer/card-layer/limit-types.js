module.exports = {
    TRANSACTION: 'transaction',
    DAILY: 'daily',
    WEEKLY: 'weekly',
    MONTHLY: 'monthly'
};