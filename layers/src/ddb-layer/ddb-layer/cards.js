const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const forwards = require('./forwards');
const sfet = require('simple-free-encryption-tool');
const uuid = require('uuid').v4;

const CARDS_TABLE = process.env.CARDS_TABLE_NAME;
const USERS_TABLE = process.env.USERS_TABLE_NAME;

const LIMIT_TYPES = require('/opt/nodejs/card-layer/limit-types');

let keyHash = ({ cardId, apiKey }) => {
    return sfet.sha256.hash(cardId + apiKey);
};

let generateApiKey = () => {
    return sfet.utils.randomstring.generate(64);
};

let checkCardDuplicate = ({ userId, cardId, cardName, cardNumber }) => {
    return new Promise((resolve, reject) => {
        dynamodb.get({
            TableName: USERS_TABLE,
            Key: {
                "userId": userId
            }
        }).promise()
        .then(user => {
            user = user.Item;
            user.cards = user.cards || [];
            if (cardId && (user.cards.indexOf(cardId) >= 0)) {
                user.cards.splice(user.cards.indexOf(cardId), 1);
            }
            if (user.cards.length == 0) {
                return resolve({ user });
            }

            let checkedCards = 0;
            for (let i in user.cards) {
                let testCardId = user.cards[i];
                dynamodb.get({
                    TableName: CARDS_TABLE,
                    Key: {
                        "cardId": testCardId
                    }
                }).promise()
                .then(testCard => {
                    testCard = testCard.Item;

                    if (testCard.cardName == cardName) {
                        return reject(new Error(`${cardName} already registered for this user`));
                    }
                    if (testCard.cardNumber.indexOf(cardNumber) > -1 ||
                        cardNumber.indexOf(testCard.cardNumber) > -1) {
                        return reject(new Error(`${cardNumber} (or similar) already registered for this user`));
                    }

                    checkedCards++;
                    // resolve if this is the last of the user's cards
                    if (checkedCards == user.cards.length) {
                        resolve({ user });
                    }
                })
                .catch(reject);
            }
        })
        .catch(reject);
    });
};

/*
    cardId - uuid to be included in payload
    cardName - a convenience name for the card
    cardNumber - last 4-6 digits
    cardLimits - an approval limits object
    checkKey (api key hashed with cardId)
*/
let self = {
    authenticate: ({ cardId, apiKey }) => {
        return new Promise((resolve, reject) => {
            dynamodb.get({
                TableName: CARDS_TABLE,
                Key: {
                    "cardId": cardId
                }
            }).promise()
            .then(result => {
                let card = result.Item;
                if (card.checkKey == keyHash({ cardId, apiKey})) {
                    resolve(card);
                } else {
                    reject();
                }
            })
            .catch(reject);
        });
    },
    createEntry: ({ userId, cardName, cardNumber }) => {
        return new Promise((resolve, reject) => {
            // store at most last six digits of card number
            cardNumber = cardNumber.substr(-6);

            // store the card details
            let storeCard = ({ user }) => {
                let cardId = uuid();
                let apiKey = generateApiKey();

                dynamodb.put({
                    TableName: CARDS_TABLE,
                    Item: {
                        "cardId": cardId,
                        "cardName": cardName,
                        "cardNumber": cardNumber,
                        "checkKey": keyHash({ cardId, apiKey}),
                        "cardLimits": {
                            preauthorized: [],
                            regular: [],
                            usage: {
                                daily: {
                                    used: 0,
                                    date: (new Date()).getDate()
                                },
                                weekly: {},
                                monthly: {
                                    used: 0,
                                    month: (new Date()).getMonth()
                                }
                            }
                        }
                    }
                }).promise()
                .then(() => {
                    user.cards.push(cardId);
                    updateUser(user);
                })
                .catch(reject);

                // update the user's cards
                let updateUser = (user) => {
                    dynamodb.update({
                        TableName: USERS_TABLE,
                        Key: {
                            "userId": userId
                        },
                        UpdateExpression: "set cards = :c",
                        ExpressionAttributeValues:{
                            ":c": user.cards
                        }
                    }).promise()
                    .then(() => { resolve({ cardId, apiKey }); })
                    .catch(reject);
                };
            };

            // ensure the card details aren't already registered
            checkCardDuplicate({ userId, cardName, cardNumber })
            .then(storeCard)
            .catch(reject);
        });
    },
    list: ({ userId }) => {
        return new Promise((resolve, reject) => {
            dynamodb.get({
                TableName: USERS_TABLE,
                Key: {
                    "userId": userId
                }
            }).promise()
            .then(user => {
                user = user.Item;
                user.cards = user.cards || [];
                if (user.cards.length == 0) {
                    return resolve([]);
                }

                let cards = [];
                let checkedCards = 0;
                for (let i in user.cards) {
                    let cardId = user.cards[i];
                    dynamodb.get({
                        TableName: CARDS_TABLE,
                        Key: {
                            "cardId": cardId
                        }
                    }).promise()
                    .then(card => {
                        card = card.Item;
                        cards.push({
                            cardId: cardId,
                            cardName: card.cardName,
                            cardNumber: card.cardNumber
                        });

                        checkedCards++;
                        if (checkedCards == user.cards.length) {
                            resolve(cards);
                        }
                    })
                    .catch(reject);
                }
            })
            .catch(reject);
        });
    },
    generateApiKey: ({ cardId, apiKey, clearForwards }) => {
        return new Promise((resolve, reject) => {
            let regenerate = (forwardsArray) => {
                let newApiKey = generateApiKey();
                let newCheckKey = keyHash({ cardId, apiKey: newApiKey});
                dynamodb.update({
                    TableName: CARDS_TABLE,
                    Key: {
                        "cardId": cardId
                    },
                    UpdateExpression: "set checkKey = :ck",
                    ExpressionAttributeValues:{
                        ":ck": newCheckKey
                    }
                }).promise()
                .then(() => {
                    forwards.createOrUpdateEntry({
                        cardId,
                        apiKey: newApiKey,
                        forwards: forwardsArray
                    })
                    .then(() => {
                        resolve(newApiKey);
                    })
                    .catch((err) => {
                        console.error(err);
                        resolve(newApiKey);
                    });
                })
                .catch(reject);
            }

            if (clearForwards) {
                regenerate([]);
            } else {
                forwards.getEntry({ cardId, apiKey })
                .then(regenerate)
                .catch(reject);
            }
        });
    },
    updateEntry: ({ userId, cardId, cardLimits, cardName, cardNumber }) => {
        return new Promise((resolve, reject) => {

            if (!userId && (cardName || cardNumber)) {
                reject(new Error("userId must be provided when updating card name or number"));
            }

            // store at most last six digits of card number
            if (cardNumber) {
                cardNumber = cardNumber.substr(-6);
            }

            dynamodb.get({
                TableName: CARDS_TABLE,
                Key: {
                    "cardId": cardId
                }
            }).promise()
            .then((card) => {
                card = card.Item;
                card.cardName = cardName || card.cardName;
                card.cardNumber = cardNumber || card.cardNumber;
                card.cardLimits = cardLimits || card.cardLimits;

                if (userId) {
                    // ensure the new card details aren't already registered
                    checkCardDuplicate({
                        userId,
                        cardId,
                        cardName: card.cardName,
                        cardNumber: card.cardNumber
                    })
                    .then(() => {
                        updateCard(card);
                    })
                    .catch(reject);
                } else {
                    updateCard(card);
                }
            })
            .catch(reject);

            let updateCard = (card) => {
                console.log(`updateEntry updating card entry ${cardId}`);
                dynamodb.update({
                    TableName: CARDS_TABLE,
                    Key: {
                        "cardId": cardId
                    },
                    UpdateExpression: "set cardName = :name, cardNumber = :num, cardLimits = :cl",
                    ExpressionAttributeValues:{
                        ":name": card.cardName,
                        ":num": card.cardNumber,
                        ":cl": card.cardLimits
                    }
                }).promise()
                .then(() => { resolve(); })
                .catch(reject);
            };
        });
    },
    updateUsage: ({ usage, centsAmount, override }) => {
        let today = (new Date()).getDate();
        let thisMonth = (new Date()).getMonth();

        let dailyUsage = usage[LIMIT_TYPES.DAILY];
        if (dailyUsage.date != today || override) {
            dailyUsage.used = centsAmount;
        } else {
            dailyUsage.used += centsAmount;
        }
        dailyUsage.date = today;

        let monthlyUsage = usage[LIMIT_TYPES.MONTHLY];
        if (monthlyUsage.month != thisMonth || override) {
            monthlyUsage.used = centsAmount;
        } else {
            monthlyUsage.used += centsAmount;
        }
        monthlyUsage.month = thisMonth;

        // TODO custom limit types

        return usage;
    }
};

module.exports = self;