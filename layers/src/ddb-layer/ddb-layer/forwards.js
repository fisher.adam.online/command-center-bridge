const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const sfet = require('simple-free-encryption-tool');

const FORWARDS_TABLE = process.env.FORWARDS_TABLE_NAME;

function validateForward(forward) {
    if (!forward.url) {
         throw new Error("all forwards must have a url");
    }
    /*if (!forward.map || typeof forward.map !== "function") {
        throw new Error(`forward map for ${forward.url} must be a function`);
    }*/
    for (let key in forward.headers) {
        if (forward.headers.hasOwnProperty(key)) {
            if (typeof forward.headers[key] !== "string") {
                throw new Error(`forward header ${key} for ${forward.url} must be a string`);
            }
        }
    }
}

/*
    cardId - uuid to be included in payload
    forwards - [{ url, map, headers}] encrypted by apiKey
*/
let self = {
    createOrUpdateEntry: ({ cardId, apiKey, forwards }) => {
        return new Promise((resolve, reject) => {
            // validate forwards
            for (let i in forwards) {
                try {
                    validateForward(forwards[i]);
                } catch (e) {
                    return reject(e);
                }
            }

            try {
                forwards = sfet.aes.encrypt(apiKey, JSON.stringify(forwards));
            } catch (err) {
                return reject(err);
            }

            dynamodb.put({
                TableName: FORWARDS_TABLE,
                Item: {
                    "cardId": cardId,
                    "forwards": forwards
                }
            }).promise()
            .then(() => {
                resolve();
            })
            .catch(reject);
        });
    },
    getEntry: ({ cardId, apiKey }) => {
        return new Promise((resolve, reject) => {
            dynamodb.get({
                TableName: FORWARDS_TABLE,
                Key: {
                    "cardId": cardId
                }
            }).promise()
            .then(result => {
                result = result.Item;
                if (result) {
                    let forwards = JSON.parse(sfet.aes.decrypt(apiKey, result.forwards));
                    resolve(forwards);
                } else {
                    resolve([]);
                }
            })
            .catch(reject);
        });
    },
    updateEntry: ({ cardId, apiKey, forward }) => {
        return new Promise((resolve, reject) => {
            return reject(new Error("not implemented"));

            try {
                validateForward(forward);
            } catch (e) {
                return reject(e);
            }

            // replace forward, encrypt forwards
            self.getEntry({ cardId, apiKey })
            .then(forwards => {
                // TODO loop through forwards array, if the url matches replace it
                self.createEntry({ cardId, apiKey, forwards })
                .then(() => { resolve(); })
                .catch(reject);
            })
            .catch(reject);
        });
    },
    deleteEntry: ({ cardId, apiKey, forward }) => {
        return new Promise((resolve, reject) => {
            return reject(new Error("not implemented"));
        });
    }
};

module.exports = self;