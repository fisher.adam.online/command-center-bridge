const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const uuid = require('uuid').v4;

const LOGIN_TABLE = process.env.LOGIN_TABLE_NAME;

let self = {
    createEntry: ({ device }) => {
        return new Promise((resolve, reject) => {
            dynamodb.put({
                TableName: LOGIN_TABLE,
                Item: {
                    "device": device,
                    "userId": uuid(),
                    "verified": false
                }
            }).promise()
            .then(() => { resolve(); })
            .catch(reject);
        });
    },
    getEntry: ({ device }) => {
        return new Promise((resolve, reject) => {
            dynamodb.get({
                TableName: LOGIN_TABLE,
                Key: {
                    "device": device
                }
            }).promise()
            .then(result => { resolve(result.Item); })
            .catch(reject);
        });
    },
    verifyEntry: ({ device }) => {
        return new Promise((resolve, reject) => {
            dynamodb.update({
                TableName: LOGIN_TABLE,
                Key: {
                    "device": device
                },
                UpdateExpression: "set verified = :v",
                ExpressionAttributeValues:{
                    ":v": true
                }
            }).promise()
            .then(result => {
                self.getEntry({ device })
                .then(resolve)
                .catch(reject);
            })
            .catch(reject);
        });
    }
};

module.exports = self;