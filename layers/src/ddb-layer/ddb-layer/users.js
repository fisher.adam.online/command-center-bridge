const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();

const USERS_TABLE = process.env.USERS_TABLE_NAME;

let self = {
    createEntry: ({ userId, device, deviceType }) => {
        return new Promise((resolve, reject) => {
            dynamodb.put({
                TableName: USERS_TABLE,
                Item: {
                    "userId": userId,
                    devices: [ { device, deviceType} ],
                    "active": true
                }
            }).promise()
            .then(() => { resolve(); })
            .catch(reject);
        });
    },
    getEntry: ({ userId }) => {
        return new Promise((resolve, reject) => {
            dynamodb.get({
                TableName: USERS_TABLE,
                Key: {
                    "userId": userId
                }
            }).promise()
            .then(result => { resolve(result.Item); })
            .catch(reject);
        });
    },
    addCard: ({ userId, card }) => {},
    removeCard: ({ userId, card }) => {},
    addDevice: ({ userId, device, deviceType }) => {},
    removeDevice: ({ userId, device }) => {},
    setPassword: ({ userId, password }) => {},
    unsetPassword: ({ userId }) => {},
    updateActiveState: ({ userId, active }) => {
        return new Promise((resolve, reject) => {
            dynamodb.update({
                TableName: USERS_TABLE,
                Key: {
                    "userId": userId
                },
                UpdateExpression: "set active = :a",
                ExpressionAttributeValues:{
                    ":a": active
                }
            }).promise()
            .then(result => { resolve(result.Item); })
            .catch(reject);
        });
    }
};

module.exports = self;