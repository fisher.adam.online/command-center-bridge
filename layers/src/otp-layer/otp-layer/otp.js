const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const sfet = require('simple-free-encryption-tool');

const DEVICE_TYPES = require('./device-types');

const OTP_TABLE = process.env.OTP_TABLE_NAME;
// TODO set with environment variable using https://github.com/zeit/ms
const OTP_TTL_IN_SECONDS = 60*5;
const OTP_EXPIRATION_MESSAGE = "OTP has expired";

// TODO add reinstantiation logic if env variables updated
const mailgun = require('mailgun-js')({
    apiKey: process.env.MAILGUN_API_KEY,
    domain: process.env.MAILGUN_DOMAIN
});

let self = {
    createEntry: ({ device, purpose }) => {
        return new Promise((resolve, reject) => {
            let otp = sfet.utils.randomstring.generate(6);
            dynamodb.put({
                TableName: OTP_TABLE,
                Item: {
                    "device": device,
                    "otp": otp,
                    "purpose": purpose,
                    "expiration": Math.floor(Date.now() / 1000) + OTP_TTL_IN_SECONDS
                }
            }).promise()
            .then(() => { resolve(otp); })
            .catch(reject);
        });
    },
    deleteEntry: ({ device, otp }) => {
        return new Promise((resolve, reject) => {
            dynamodb.delete({
                TableName: OTP_TABLE,
                Key: {
                    "device": device,
                    "otp": otp
                }
            }).promise()
            .then(resolve)
            .catch(reject);
        });
    },
    getEntry: ({ device, otp }) => {
        return new Promise((resolve, reject) => {
            let otpRetrievalFailure = (err) => {
                console.error(err);
                reject(new Error(OTP_EXPIRATION_MESSAGE));
            };
            dynamodb.get({
                TableName: OTP_TABLE,
                Key: {
                    "device": device,
                    "otp": otp
                }
            }).promise()
            .then(result => {
                result = result.Item;
                if (!result) {
                    otpRetrievalFailure(new Error("otp not found"));
                } else {
                    // validate expiration time
                    let currentTime = Math.floor(Date.now() / 1000);
                    if (currentTime > result.expiration) {
                        otpRetrievalFailure(
                            new Error(`otp expired: ${currentTime} > ${result.expiration}`)
                        );
                    } else {
                        resolve(result);
                    }
                }
            })
            .catch(otpRetrievalFailure);
        });
    },
    send: ({ deviceType, device, subject, body }) => {
        return new Promise((resolve, reject) => {
            if (deviceType == DEVICE_TYPES.EMAIL) {
                let message = {
                    from: process.env.MAILGUN_FROM,
                    to: device,
                    subject: subject,
                    text: body
                };

                mailgun.messages().send(message, function (err, body) {
                    if (err){
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            } else {
                // TODO implement mobile OTPs
                reject("mobile authentication not implemented");
            }
        });
    }
};

module.exports = self;