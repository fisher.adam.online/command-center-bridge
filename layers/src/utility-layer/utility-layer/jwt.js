const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const jwt = require("jsonwebtoken");

const LOGIN_TABLE = process.env.LOGIN_TABLE_NAME;

let self = {
    authenticateToken: ({ requestHeaders }) => {
        return new Promise((resolve, reject) => {
            let authHeader = requestHeaders['Authorization']
            let token = authHeader && authHeader.split(' ')[1]
            if (token == null) {
                return reject({
                    statusCode: 401,
                    message: "authentication token required"
                });
            }

            jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
                if (err) {
                    console.error(err);
                    return reject({
                        statusCode: 401,
                        message: "invalid/expired authentication token"
                    });
                }
                resolve(user);
            });
        });
    },
    generateToken: ({ userId, device }) => {
        return new Promise((resolve, reject) => {
            let tokenGenerator = ({ userId }) => {
                // TODO calculate expiration time using https://github.com/zeit/ms
                jwt.sign(
                    { userId },
                    process.env.JWT_SECRET,
                    { expiresIn: process.env.JWT_EXPIRATION },
                    (err, token) => {
                        if (err) {
                            console.error(err);
                            return reject(new Error("Error signing token"));
                        }
                        resolve(token);
                    }
                );
            };
            if (userId) {
                tokenGenerator({ userId });
            } else {
                // retrieve user id
                dynamodb.get({
                    TableName: LOGIN_TABLE,
                    Key: {
                        "device": device
                    }
                }).promise()
                .then(result => {
                    tokenGenerator({ userId: result.Item.userId });
                })
                .catch(reject);
            }
        });
    }
};

module.exports = self;