import * as cdk from '@aws-cdk/core';
import { Duration } from '@aws-cdk/core';
import { RestApi, LambdaIntegration, Cors } from '@aws-cdk/aws-apigateway';
import { Table, AttributeType, BillingMode } from '@aws-cdk/aws-dynamodb';
import { Rule, Schedule } from '@aws-cdk/aws-events';
import { LambdaFunction } from '@aws-cdk/aws-events-targets';
import { Function, Runtime, Code, LayerVersion } from '@aws-cdk/aws-lambda';
import { Queue, QueueEncryption } from '@aws-cdk/aws-sqs';
import { SqsEventSource } from '@aws-cdk/aws-lambda-event-sources';

import * as crypto from "crypto";

import credentialsJson from './credentials.json';
type credentialsType = {
  [key: string]: any
}

let credentials: credentialsType = credentialsJson;

const DDB_ACCESS = {
  READ: 'read',
  WRITE: 'write',
  FULL: 'full'
};

type ddbAccessConfiguration = {
  table: Table,
  access: string
}

function createIntegratedFunction(stack: CommandCenterBridgeStack, definition: any) {
  let lambda = new Function(stack, `${definition.name}-function`, {
    runtime: Runtime.NODEJS_12_X,
    handler: definition.handler,
    code: Code.fromAsset(definition.code),
    environment: definition.environment,
    layers: definition.layers,
    timeout: definition.timeout || Duration.seconds(5)
  });

  if (definition.resource) {
    definition.resource.addMethod(definition.method, new LambdaIntegration(lambda));
  }

  let ddbAccessConfigurations: ddbAccessConfiguration[] = definition.ddbAccess || [];
  for (let dai in ddbAccessConfigurations) {
    let ddbAccessConfiguration = ddbAccessConfigurations[dai];
    switch (ddbAccessConfiguration.access) {
      case DDB_ACCESS.FULL:
        ddbAccessConfiguration.table.grantFullAccess(lambda);
        break;
      case DDB_ACCESS.READ:
        ddbAccessConfiguration.table.grantReadData(lambda);
        break;
      case DDB_ACCESS.WRITE:
        ddbAccessConfiguration.table.grantWriteData(lambda);
        break;
    }
  }

  let queueAccess = definition.queueAccess || [];
  for (let qai in queueAccess) {
    definition.queueAccess[qai].grantSendMessages(lambda);
  }

  let eventSources = definition.eventSources || [];
  for (let esi in eventSources) {
    lambda.addEventSource(eventSources[esi]);
  }

  if (definition.cron) {
    let rule = new Rule(stack, `${definition.name}-rule`, {
      schedule: definition.cron
    });

    rule.addTarget(new LambdaFunction(lambda));
  }
}

export class CommandCenterBridgeStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps, corsOrigin?: string) {
    super(scope, id, props);
    const stack = this;
    corsOrigin = corsOrigin || "*";

    let corsEnvironment = {
      CORS_ORIGIN: corsOrigin
    };

    /*
      login ddb table
        device: email/mobile
          - if a logged in user adds an auth factor, user will be assigned additional entries
          - TBD if a logged in user could merge auth factors (would have to deactivate and recreate)
          - TBD always require logging in with a different device if available
        userId - uuid
        verified - bool
    */
    const ccbLoginTable = new Table(stack, 'ccb-login', {
      partitionKey: { name: 'device', type: AttributeType.STRING },
      billingMode: BillingMode.PAY_PER_REQUEST
    });

    /*
      users ddb table
        userId - uuid
        password - optional, string hashed with userId and salt
        salt - optional, random string
        devices: [string]
        active - bool
    */
   const ccbUsersTable = new Table(stack, 'ccb-users', {
    partitionKey: { name: 'userId', type: AttributeType.STRING },
    billingMode: BillingMode.PAY_PER_REQUEST
  });

  /*
      otp table
        device - email/mobile
        otp - variable size unambiguous characters
        purpose
        TTL on expiration field
    */
    const ccbOtpTable = new Table(stack, 'ccb-otp', {
      partitionKey: { name: 'device', type: AttributeType.STRING },
      sortKey: { name: 'otp', type: AttributeType.STRING },
      billingMode: BillingMode.PAY_PER_REQUEST,
      timeToLiveAttribute: 'expiration'
    });

    /*
      cards ddb table
        cardId - uuid to be included in payload
        cardName - name on card
        cardNumber - last 4-6 digits of the number on the card
        checkKey (api key hashed with cardId)
    */
    const ccbCardsTable = new Table(stack, 'ccb-cards', {
      partitionKey: { name: 'cardId', type: AttributeType.STRING },
      billingMode: BillingMode.PAY_PER_REQUEST
    });

    /*
      forwards ddb table
        cardId
        forwards - [{ url, map, headers}]
    */
    const ccbForwardsTable = new Table(stack, 'ccb-forwards', {
      partitionKey: { name: 'cardId', type: AttributeType.STRING },
      billingMode: BillingMode.PAY_PER_REQUEST
    });

    // transaction forwarding sqs queue
    const sqsForwardingQueue = new Queue(this, 'ccb-forwarding-queue', {
      encryption: QueueEncryption.KMS_MANAGED
    });

    const sqsForwardingQueueEventSource = new SqsEventSource(sqsForwardingQueue, {
      batchSize: 10 // default is 10
    });

    // utility layer
    const utilityLayer = new LayerVersion(stack, 'utility-layer', {
      // Code.fromAsset must reference the build folder
      code: Code.fromAsset('./layers/build/utility-layer'),
      compatibleRuntimes: [Runtime.NODEJS_12_X],
      license: 'MIT',
      description: 'a layer for providing utility functions',
    });

    // otp layer
    const otpLayer = new LayerVersion(stack, 'otp-layer', {
      // Code.fromAsset must reference the build folder
      code: Code.fromAsset('./layers/build/otp-layer'),
      compatibleRuntimes: [Runtime.NODEJS_12_X],
      license: 'MIT',
      description: 'a layer for providing OTP / MFA functionality',
    });

    // ddb layer
    const ddbLayer = new LayerVersion(stack, 'ddb-layer', {
      // Code.fromAsset must reference the build folder
      code: Code.fromAsset('./layers/build/ddb-layer'),
      compatibleRuntimes: [Runtime.NODEJS_12_X],
      license: 'MIT',
      description: 'a layer for providing dynamodb table interfaces',
    });

    // card layer
    const cardLayer = new LayerVersion(stack, 'card-layer', {
      // Code.fromAsset must reference the build folder
      code: Code.fromAsset('./layers/build/card-layer'),
      compatibleRuntimes: [Runtime.NODEJS_12_X],
      license: 'MIT',
      description: 'a layer for card manipulation, authorization and transaction forwarding',
    });

    // configure JWT secret and expiration
    let jwtSecret = crypto.randomBytes(64).toString('hex');
    // see https://github.com/zeit/ms
    let jwtExpiration = "30m";

    const ccbApi = new RestApi(stack, `ccb-api`, {
      defaultCorsPreflightOptions: {
        allowOrigins: [ corsOrigin ],
        allowMethods: Cors.ALL_METHODS,
      }
    });

    // set up api resources
    const api: any = {
      'root': ccbApi.root
    };

    // /register
    api.register = api.root.addResource('register');

    // /auth
    api.auth = api.root.addResource('auth');
    // /auth/login
    api.login = api.auth.addResource('login');

    let authEnvironmentBase = {
      LOGIN_TABLE_NAME: ccbLoginTable.tableName,
      USERS_TABLE_NAME: ccbUsersTable.tableName,
      OTP_TABLE_NAME: ccbOtpTable.tableName
    };

    let mailgunEnvironment = {
      MAILGUN_DOMAIN: credentials.mailgun.domain,
      MAILGUN_FROM: credentials.mailgun.from,
      MAILGUN_API_KEY: credentials.mailgun.api_key
    }

    let jwtEnvironment = {
      JWT_SECRET: jwtSecret,
      JWT_EXPIRATION: jwtExpiration
    }

    let authFunctions = [
      {
        name: 'user-registration',
        handler: 'index.register',
        code: './handlers/user-registration',
        method: 'POST',
        resource: api.register,
        environment: {
          ...corsEnvironment,
          ...authEnvironmentBase,
          ...mailgunEnvironment
        },
        ddbAccess: [
          { table: ccbLoginTable, access: DDB_ACCESS.FULL },
          { table: ccbOtpTable, access: DDB_ACCESS.WRITE }
        ],
        layers: [otpLayer, ddbLayer, utilityLayer],
      },
      {
        name: 'user-verification',
        handler: 'index.verify',
        code: './handlers/user-registration',
        method: 'PUT',
        resource: api.register,
        environment: {
          ...corsEnvironment,
          ...authEnvironmentBase,
          ...mailgunEnvironment
        },
        ddbAccess: [
          { table: ccbLoginTable, access: DDB_ACCESS.FULL },
          { table: ccbUsersTable, access: DDB_ACCESS.WRITE },
          { table: ccbOtpTable, access: DDB_ACCESS.FULL }
        ],
        layers: [otpLayer, ddbLayer, utilityLayer]
      },
      {
        name: 'user-deregistration',
        handler: 'index.deregister',
        code: './handlers/user-registration',
        method: 'DELETE',
        resource: api.register,
        environment: {
          ...corsEnvironment,
          ...authEnvironmentBase,
          ...mailgunEnvironment
        },
        ddbAccess: [
          { table: ccbLoginTable, access: DDB_ACCESS.FULL }
        ],
        layers: [otpLayer, ddbLayer, utilityLayer]
      },
      {
        name: 'user-login',
        handler: 'index.login',
        code: './handlers/user-authentication',
        method: 'POST',
        resource: api.login,
        environment: {
          ...corsEnvironment,
          ...authEnvironmentBase,
          ...mailgunEnvironment
        },
        ddbAccess: [
          { table: ccbLoginTable, access: DDB_ACCESS.READ },
          { table: ccbUsersTable, access: DDB_ACCESS.FULL },
          { table: ccbOtpTable, access: DDB_ACCESS.WRITE }
        ],
        layers: [otpLayer, ddbLayer, utilityLayer]
      },
      {
        name: 'user-authentication',
        handler: 'index.authenticate',
        code: './handlers/user-authentication',
        method: 'PUT',
        resource: api.login,
        environment: {
          ...corsEnvironment,
          ...authEnvironmentBase,
          ...jwtEnvironment,
          ...mailgunEnvironment
        },
        ddbAccess: [
          { table: ccbLoginTable, access: DDB_ACCESS.READ },
          { table: ccbOtpTable, access: DDB_ACCESS.FULL }
        ],
        layers: [otpLayer, ddbLayer, utilityLayer]
      }
    ];

    for (let afi in authFunctions) {
      createIntegratedFunction(stack, authFunctions[afi]);
    }

    // api definitions for jwt functions
    // /auth/refresh
    api.refresh = api.auth.addResource('refresh');

    // /cards
    api.cards = api.root.addResource('cards');
    // /cards/{cardId}
    api.card = api.cards.addResource('{cardId}');
    // /cards/{cardId}/api-key
    api.cardApiKey = api.card.addResource('api-key');
    // /cards/{cardId}/api-key/renew
    api.generateApiKey = api.cardApiKey.addResource('renew');
    // /cards/{cardId}/forwards
    api.cardForwards = api.card.addResource('forwards');

    let cardEnvironment = {
      USERS_TABLE_NAME: ccbUsersTable.tableName,
      CARDS_TABLE_NAME: ccbCardsTable.tableName,
      FORWARDS_TABLE_NAME: ccbForwardsTable.tableName
    };

    let transactionEnvironment = {
      FORWARDS_TABLE_NAME: ccbForwardsTable.tableName,
      TRANSACTION_FORWARD_QUEUE_URL: sqsForwardingQueue.queueUrl
    };

    let jwtProtectedFunctions = [
      {
        name: 'user-token-refresh',
        handler: 'index.refreshToken',
        code: './handlers/jwt',
        resource: api.refresh,
        method: 'GET',
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment
        },
        layers: [utilityLayer]
      },
      {
        name: 'cards-list',
        handler: 'index.listCards',
        code: './handlers/cards',
        method: 'GET',
        resource: api.cards,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbCardsTable, access: DDB_ACCESS.READ },
          { table: ccbUsersTable, access: DDB_ACCESS.READ }
        ]
      },
      {
        name: 'card-add',
        handler: 'index.addCard',
        code: './handlers/cards',
        method: 'POST',
        resource: api.cards,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbCardsTable, access: DDB_ACCESS.FULL },
          { table: ccbUsersTable, access: DDB_ACCESS.FULL }
        ]
      },
      {
        name: 'card-renew-api-key',
        handler: 'index.generateApiKey',
        code: './handlers/cards',
        method: 'GET',
        resource: api.generateApiKey,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbCardsTable, access: DDB_ACCESS.FULL },
          { table: ccbForwardsTable, access: DDB_ACCESS.FULL }
        ]
      },
      {
        name: 'card-forwards-list',
        handler: 'index.listForwards',
        code: './handlers/forwarding',
        method: 'GET',
        resource: api.cardForwards,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbForwardsTable, access: DDB_ACCESS.READ }
        ]
      },
      {
        name: 'card-forwards-store',
        handler: 'index.storeForwards',
        code: './handlers/forwarding',
        method: 'POST',
        resource: api.cardForwards,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbForwardsTable, access: DDB_ACCESS.WRITE }
        ]
      }
    ];

    for (let jpi in jwtProtectedFunctions) {
      createIntegratedFunction(stack, jwtProtectedFunctions[jpi]);
    }

    // API key protected

    // /cards/{cardId}/approve
    api.cardApproval = api.card.addResource('approve');
    // /cards/{cardId}/relay
    api.cardRelay = api.card.addResource('relay');
    // /cards/{cardId}/limits
    api.cardLimits = api.card.addResource('limits');
    // /cards/{cardId}/preauthorization
    api.cardPreauthorization = api.card.addResource('preauthorization');
    // /cards/{cardId}/used
    api.cardUsed = api.card.addResource('used');

    let apiKeyProtectedFunctions = [
      {
        name: 'card-approve',
        handler: 'approver.approve',
        code: './handlers/cards',
        method: 'POST',
        resource: api.cardApproval,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment,
          ...transactionEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbCardsTable, access: DDB_ACCESS.READ }
        ],
        queueAccess: [
          sqsForwardingQueue
        ],
        // recommended period for warming up functions is 15 minutes
        cron: Schedule.rate(Duration.minutes(15))
      },
      {
        name: 'card-relay',
        handler: 'relayer.relay',
        code: './handlers/cards',
        method: 'POST',
        resource: api.cardRelay,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment,
          ...transactionEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbCardsTable, access: DDB_ACCESS.FULL }
        ],
        queueAccess: [
          sqsForwardingQueue
        ]
      },
      {
        name: 'card-set-limits',
        handler: 'index.setCardLimits',
        code: './handlers/cards',
        method: 'POST',
        resource: api.cardLimits,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbCardsTable, access: DDB_ACCESS.FULL }
        ]
      },
      {
        name: 'card-get-limits',
        handler: 'index.getCardLimits',
        code: './handlers/cards',
        method: 'GET',
        resource: api.cardLimits,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbCardsTable, access: DDB_ACCESS.READ }
        ]
      },
      {
        name: 'card-add-preauthorization',
        handler: 'index.addPreauthorization',
        code: './handlers/cards',
        method: 'POST',
        resource: api.cardPreauthorization,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbCardsTable, access: DDB_ACCESS.FULL }
        ]
      },
      {
        name: 'card-update-used',
        handler: 'index.updateUsage',
        code: './handlers/cards',
        method: 'POST',
        resource: api.cardUsed,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbCardsTable, access: DDB_ACCESS.FULL }
        ]
      },
      {
        name: 'card-get-usage',
        handler: 'index.getUsage',
        code: './handlers/cards',
        method: 'GET',
        resource: api.cardUsed,
        environment: {
          ...corsEnvironment,
          ...jwtEnvironment,
          ...cardEnvironment
        },
        layers: [ cardLayer, ddbLayer, utilityLayer],
        ddbAccess: [
          { table: ccbCardsTable, access: DDB_ACCESS.FULL }
        ]
      }
    ];

    for (let akpi in apiKeyProtectedFunctions) {
      createIntegratedFunction(stack, apiKeyProtectedFunctions[akpi]);
    }

    // internal unprotected
    let internalUnprotectedFunctions = [
      {
        name: 'transaction-forwarding',
        handler: 'index.forwarder',
        code: './handlers/forwarding',
        environment: {
          ...corsEnvironment,
          FORWARDS_TABLE_NAME: ccbForwardsTable.tableName
        },
        layers: [ ddbLayer, utilityLayer ],
        eventSources: [ sqsForwardingQueueEventSource ],
        ddbAccess: [
          { table: ccbForwardsTable, access: DDB_ACCESS.READ }
        ],
        timeout: Duration.seconds(20)
      }
    ];

    for (let iui in internalUnprotectedFunctions) {
      createIntegratedFunction(stack, internalUnprotectedFunctions[iui]);
    }
  }
}
